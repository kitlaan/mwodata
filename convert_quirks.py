#!/usr/bin/env python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from collections import OrderedDict
import json
import os
import sys

schema = 1

typolist = {
        'ASN-21S':     'ASN-21',      # someone forgot to mark it Special
        'CN9-ANCIX':   'CN9-A',       # just easier to fix it with a typo
        'HBK-IICC-DW': 'HBK-IIC-DW',  # someone has sticky fingers
        'MAD-IIC2':    'MAD-IIC-2',   # typo
        'NTG-K':       'NTG-JK',      # typo in ntg-jk.mdf
        'SMN-FL':      'SMN-F',       # typo in summoner-omnipods.xml
        'SMN-ML':      'SMN-M',       # typo in summoner-omnipods.xml
        'WHM-PRIME':   'WHM-IIC',     # no clue why they did this
        'TDR-5SSPEC':  'TDR-5S',      # someone forgot to use the normal style
        }

def fixname(name, variant):
    if '(' in name:
        name = name.split('(')[0]
    elif name.endswith('-STD'):
        # damn you e.g. "AS7-S-STD"
        name = name[:-4]
    elif variant and name[-2] != '-':
        # damn you "AS7-S" with Variant == "Special"
        if variant == 'Special':
            if endswithany(name, ('L', 'S', 'I', 'G', 'O', 'R')):
                name = name[:-1]
        elif variant == 'Champion':
            if endswithany(name, ('C')):
                name = name[:-1]
        elif variant == 'Phoenix':
            if endswithany(name, ('P')):
                name = name[:-1]

    return name

def endswithany(string, anylist):
    for a in anylist:
        if string.endswith(a):
            return True
    return False

class MechDefinitionHandler(ContentHandler):
    def __init__(self):
        self.mechs = {}

        self.name = None
        self.working = None

        self.nodes = []
        self.inComponent = None

    def checkNode(self, *args):
        return self.nodes == list(args)

    def initMech(self, name, variant, origin):
        fname = fixname(name.upper(), variant)

        # and of course, there's typos
        if fname in typolist:
            fname = typolist[fname]

        self.name = fname
        self.working = {}

    def addMech(self):
        if self.name:
            if self.name not in self.mechs or len(self.mechs[self.name]) == 0:
                self.mechs[self.name] = self.working

        self.name = None
        self.working = None

    def addQuirk(self, area, name, value):
        # typos
        if value.startswith('-.'):
            value = '-0.' + value[2:]
        elif value.startswith('.'):
            value = '0' + value

        if self.working is not None:
            if area not in self.working:
                self.working[area] = {}
            self.working[area][name] = value

    def setOmni(self, isOmni):
        if self.working is not None:
            self.working['omni'] = isOmni

    def startElement(self, name, attrs):
        self.nodes.append(name)

        if self.checkNode('MechDefinition', 'Mech'):
            variant = attrs['VariantType'] if 'VariantType' in attrs else None
            self.initMech(attrs['Variant'], variant, 'MechDefinition')
        elif self.checkNode('MechDefinition', 'QuirkList'):
            #self.setOmni(False)
            pass
        elif self.checkNode('MechDefinition', 'QuirkList', 'Quirk'):
            self.addQuirk('set', attrs['name'], attrs['value'])
        elif self.checkNode('OmniPods', 'Set'):
            self.initMech(attrs['name'], None, 'OmniPods')
            self.setOmni(True)
        elif self.checkNode('OmniPods', 'Set', 'SetBonuses', 'Bonus', 'Quirk'):
            self.addQuirk('set', attrs['name'], attrs['value'])
        elif self.checkNode('OmniPods', 'Set', 'component'):
            self.inComponent = attrs['name']
        elif self.checkNode('OmniPods', 'Set', 'component', 'Quirk') and self.inComponent:
            self.addQuirk(self.inComponent, attrs['name'], attrs['value'])

    def endElement(self, name):
        if self.checkNode('MechDefinition'):
            self.addMech()
        elif self.checkNode('OmniPods', 'Set'):
            self.addMech()
        elif self.checkNode('OmniPods', 'Set', 'component') and self.inComponent:
            self.inComponent = None

        self.nodes.pop()


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("usage: {} <ver> <datapath>".format(sys.argv[0]))
        sys.exit(1)

    saxparser = make_parser()

    quirks = MechDefinitionHandler()
    saxparser.setContentHandler(quirks)

    version = sys.argv[1]
    workpath = sys.argv[2]

    for entry in os.scandir(workpath):
        if entry.is_file() and (entry.name.endswith('.mdf') or entry.name.endswith('.xml')):
            saxparser.parse(open(os.path.join(workpath,entry.name), 'r'))

    #data = OrderedDict([
    #    ('schema', schema), ('version', version), ('date', ''),
    #    ('mechs', OrderedDict([(x, quirks.mechs[x]) for x in sorted(quirks.mechs)]))])
    #print(json.dumps(data, indent=2))
    print(json.dumps({
        'schema': schema,
        'version': version,
        'date': '',
        'mechs': quirks.mechs
        }, indent=2, sort_keys=True))
