
function fetchJSON(url, timeout, fn) {
    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", url, true);
    xr.timeout = timeout;
    xr.ontimeout = xr.onerror = function() {
        fn(null);
    };
    xr.onload = function() {
        if (xr.status == 200) {
            fn(JSON.parse(xr.responseText));
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?#&]' + name + '=([^&#]*)');

    var search = regex.exec(location.search);
    if (search != null) {
        return decodeURIComponent(search[1].replace(/\+/g, ' '));
    }

    var hash = regex.exec(location.hash);
    if (hash != null) {
        return decodeURIComponent(hash[1].replace(/\+/g, ' '));
    }

    return '';
};

///////////////////////////////////////////////////////////

function QuirkList() {
    this.quirks = {};
};

QuirkList.prototype.addQuirk = function(quirk, quirktext) {
    this.quirks[quirk] = quirktext;
};

QuirkList.prototype.getName = function(quirk) {
    if (quirk in this.quirks) {
        return this.quirks[quirk];
    }
    else {
        return quirk;
    }
};

QuirkList.prototype.formatValue = function(name, value) {
    var suffix = "";

    var prefix = "";
    if (value > 0) {
        prefix = "+";
    }
    else if (value < 0) {
        prefix = "\u2212";
        value = -value;
    }

    if (name.endsWith("%")) {
        name = name.slice(0, -2);
        suffix = "%";
        value *= 100.0;
    }
    else if (name.endsWith("+")) {
        name = name.slice(0, -2);
    }

    value = parseFloat(value).toPrecision(5).replace(/0+$/, '').replace(/\.$/, '');

    return [name, prefix + value + suffix];
};

///////////////////////////////////////////////////////////

function Mech(name, data) {
    this.name = name;
    this.data = data;
};

Mech.prototype.prefixEqual = function(other) {
    if (!other) {
        return false;
    }

    var othernames = other.name.split('-');
    var thisnames = this.name.split('-');

    var otherbase = othernames[0];
    if (othernames[1] == "IIC") {
        otherbase += "-IIC";
    }

    var thisbase = thisnames[0];
    if (thisnames[1] == "IIC") {
        thisbase += "-IIC";
    }

    return otherbase == thisbase;
};

Mech.prototype.collateQuirks = function(quirknames) {
    var quirks = {};
    for (var i of ["set", "centre_torso", "head",
                   "left_arm", "left_leg", "left_torso",
                   "right_arm", "right_leg", "right_torso"]) {
        if (i in this.data) {
            for (var q in this.data[i]) {
                var qn = q;
                if (quirknames) {
                    qn = quirknames.getName(q);
                }

                if (!(qn in quirks)) {
                    quirks[qn] = 0;
                }

                quirks[qn] += parseFloat(this.data[i][q]);
            }
        }
    }

    return quirks;
};

///////////////////////////////////////////////////////////

function MechList() {
    this.versions = {};
};

MechList.prototype.getAllMechNames = function() {
    var names = {};

    for (var v in this.versions) {
        for (var m in this.versions[v]) {
            names[m] = 0;
        }
    }

    return Object.keys(names).sort(function(a, b) {
        return a.localeCompare(b);
    });
};

MechList.prototype.getMech = function(name, version) {
    if (version in this.versions) {
        if (name in this.versions[version]) {
            return this.versions[version][name];
        }
    }

    return null;
};

MechList.prototype.addMech = function(name, version, data) {
    if (!(version in this.versions)) {
        this.versions[version] = {};
    }

    this.versions[version][name] = new Mech(name, data);
};

///////////////////////////////////////////////////////////

function Model(view) {
    this.quirklist = new QuirkList();
    this.mechlist = new MechList();
    this.versions = [];

    this.oldver = null;
    this.newver = null;

    this.loadState = 0;
};

Model.prototype.hookup = function() {
    var oldselector = document.querySelector("#oldver");
    oldselector.onchange = this.selectChange.bind(this);

    var newselector = document.querySelector("#newver");
    newselector.onchange = this.selectChange.bind(this);
};

Model.prototype.start = function() {
    fetchJSON("json/index.json", 5000, (function(data) {
        if (data) {
            for (var i in data) {
                this.versions.push([data[i].date, i]);
            }
            this.versions.sort().reverse();

            this.populateVersions(this);

            // TODO: for now, load all the quirks. but we really should just do
            // a load on selection of the versions that we want to compare
            this.loadAllVersions();
            // need to keep track of which versions we're in-process of fetching
            // and which we've already fetched (and failed)
        }
        else {
            this.loadError("index");
        }
    }).bind(this));
};

Model.prototype.loadAllVersions = function() {
    for (var v of this.versions) {
        let ver = v[1];

        if (this.oldver == null) {
            this.oldver = ver;
            this.newver = ver;
        }

        fetchJSON("json/" + ver + "/quirks.json", 5000, (function(data) {
            if (data && data.schema == 1) {
                for (var i in data.mechs) {
                    this.mechlist.addMech(i, ver, data.mechs[i]);
                }

                this.loadState++;
                this.checkLoadState();
            }
            else {
                this.loadError(ver + "/quirks.json");
            }
        }).bind(this));

        fetchJSON("json/" + ver + "/quirknames.json", 5000, (function(data) {
            if (data) {
                for (var i in data) {
                    this.quirklist.addQuirk(i, data[i]);
                }

                this.loadState++;
                this.checkLoadState();
            }
            else {
                this.loadError(ver + "/quirknames.json");
            }
        }).bind(this));
    }
};

Model.prototype.loadError = function(err) {
    this.populateError(err);
};

Model.prototype.checkLoadState = function() {
    if (this.loadState != this.versions.length * 2) {
        return;
    }

    this.populatePage(this);
};

Model.prototype.selectChange = function(e) {
    if (e.target && e.target.value) {
        var changed = false;

        var target = e.target;
        if (target.id == "oldver") {
            //this.loadVersion(target.value);
            if (this.oldver != target.value) {
                this.oldver = target.value;
                changed = true;
            }
        }
        else if (target.id == "newver") {
            //this.loadVersion(target.value);
            if (this.newver != target.value) {
                this.newver = target.value;
                changed = true;
            }
        }

        if (changed) {
            // refresh the mechs in the list
            var mlist = document.querySelectorAll("#mechlist ul li");
            for (var e of mlist) {
                var quirks = this.compareMech(e.textContent).filter(x => x.o != x.n);
                if (quirks.length > 0) {
                    e.style.color = '#fff';
                }
                else {
                    e.style.color = '';
                }
            }

            // refresh all the mechs already selected
            var mechs = document.querySelectorAll("#mechset div.mechname");
            for (var e of mechs) {
                this.buildMech(e.textContent);
            }
        }
    }
};

Model.prototype.selectMech = function(e) {
    if (e.target && e.target.onclick) {
        var name = e.target.textContent;

        var wasActive = e.target.className == "active";
        e.target.className = wasActive ? "" : "active";

        var mech = document.querySelector("#mech_" + name);
        if (wasActive) {
            if (mech) {
                mech.parentElement.removeChild(mech);
            }
            return;
        }

        var content = document.querySelector("#mechset");

        mech = this.buildMech(name);
        if (mech) {
            // do I really want to sort these?
            var mechid = "mech_" + name;
            for (var x of content.children) {
                if (x.id.localeCompare(mechid) > 0) {
                    content.insertBefore(mech, x);
                    mech = null;
                    break;
                }
            }
            if (mech) {
                content.appendChild(mech);
            }
        }
    }
};

Model.prototype.populateVersions = function() {
    var oldselector = document.querySelector("#oldver");
    var newselector = document.querySelector("#newver");

    function makeOpt(value, text) {
        var opt = document.createElement("option");
        opt.value = value;
        opt.text = value + " (" + text + ")";

        return opt;
    }

    for (var i of this.versions) {
        oldselector.options.add(makeOpt(i[1], i[0]));
        newselector.options.add(makeOpt(i[1], i[0]));
    }
};

Model.prototype.populatePage = function() {
    var base = document.querySelector("#content");

    base.style.visibility = "hidden";

    var mechnames = this.mechlist.getAllMechNames();

    var list = document.querySelector("#mechlist ul");
    for (var i of mechnames) {
        var e = document.createElement("li");
        e.textContent = i;
        e.onclick = this.selectMech.bind(this);

        list.appendChild(e);
    }

    this.removeSpinny();

    base.style.visibility = "visible";
};

Model.prototype.populateError = function(err) {
    this.removeSpinny();

    // TODO: more fancy
    var base = document.querySelector("#content");
    base.style.fontSize = "18pt";
    base.style.textAlign = "center";

    base.textContent = "error loading: " + err;
};

Model.prototype.removeSpinny = function() {
    var l = document.querySelector("#loader");
    l.parentElement.removeChild(l);
};

Model.prototype.compareMech = function(name) {
    var oldmech = this.mechlist.getMech(name, this.oldver);
    var newmech = this.mechlist.getMech(name, this.newver);

    var oldquirks = oldmech ? oldmech.collateQuirks(this.quirklist) : {};
    var newquirks = newmech ? newmech.collateQuirks(this.quirklist) : {};

    var quirks = Object.keys(newquirks).concat(Object.keys(oldquirks)).sort();

    var entries = [];
    var lastq = null;
    for (var i of quirks) {
        if (i == lastq) {
            continue;
        }
        lastq = i;

        var oldvalue = null;
        if (i in oldquirks) {
            oldvalue = oldquirks[i];
        }

        var newvalue = null;
        if (i in newquirks) {
            newvalue = newquirks[i];
        }

        entries.push({'k': i, 'o': oldvalue, 'n': newvalue});
    }

    return entries;
};

Model.prototype.buildMech = function(name) {
    var mechid = "mech_" + name;

    var ed = document.querySelector("#" + mechid);
    var e = undefined;
    if (!ed) {
        var tt = document.querySelector("#mechTemplate");

        e = document.importNode(tt.content, true);

        ed = e.querySelector("div");
        ed.id = mechid;

        var mn = e.querySelector("div.mechname");
        mn.textContent = name;

        var mi = e.querySelector("div.mechinfo");
        // mi.textContent = '\uPICKANX';
        // TODO: refactor selectMech() so I can specify a name to deal with
        // and thus hook an onclick() here to call it
    }

    var tb = ed.querySelector("tbody");
    while (tb.children.length > 0) {
        tb.removeChild(tb.firstChild);
    }

    var qt = document.querySelector("#quirkTemplate");
    var quirks = this.compareMech(name);
    for (var i of quirks) {
        var oldvalue = i.o ? this.quirklist.formatValue(i.k, i.o) : null;
        var newvalue = i.n ? this.quirklist.formatValue(i.k, i.n) : null;

        var qe = document.importNode(qt.content, true);
        var td = qe.querySelectorAll("td");
        td[0].textContent = newvalue ? newvalue[0] : oldvalue[0];
        if (newvalue && oldvalue) {
            td[1].textContent = oldvalue[1];
            if (newvalue[1] == oldvalue[1]) {
                td[1].colSpan = 2;

                td[2].parentNode.removeChild(td[2]);
            }
            else {
                td[2].textContent = newvalue[1];
            }
        }
        else if (newvalue) {
            td[1].style.color = '#a66';
            td[1].textContent = '\u2205';

            td[2].textContent = newvalue[1];
        }
        else if (oldvalue) {
            td[1].textContent = oldvalue[1];

            td[2].style.color = '#a66';
            td[2].textContent = '\u2205';
        }

        tb.appendChild(qe);
    }

    if (Object.keys(quirks).length == 0) {
        var nothing = document.querySelector("#quirkNothing");
        var qe = document.importNode(nothing.content, true);
        tb.appendChild(qe);
    }

    return e;
};

///////////////////////////////////////////////////////////

window.addEventListener('load', function() {
    var model = new Model();

    model.hookup();
    model.start();
});
