
function fetchJSON(url, timeout, fn) {
    var xr = new XMLHttpRequest();
    xr.overrideMimeType("application/json");
    xr.open("GET", url, true);
    xr.timeout = timeout;
    xr.ontimeout = xr.onerror = function() {
        fn(null);
    };
    xr.onload = function() {
        if (xr.status == 200) {
            fn(JSON.parse(xr.responseText));
        }
        else {
            xr.onerror();
        }
    };
    xr.send();
};

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?#&]' + name + '=([^&#]*)');

    var search = regex.exec(location.search);
    if (search != null) {
        return decodeURIComponent(search[1].replace(/\+/g, ' '));
    }

    var hash = regex.exec(location.hash);
    if (hash != null) {
        return decodeURIComponent(hash[1].replace(/\+/g, ' '));
    }

    return '';
};

///////////////////////////////////////////////////////////

function QuirkList() {
    this.quirks = {};
};

QuirkList.prototype.addQuirk = function(quirk, quirktext) {
    this.quirks[quirk] = quirktext;
};

QuirkList.prototype.getName = function(quirk) {
    if (quirk in this.quirks) {
        return this.quirks[quirk];
    }
    else {
        return quirk;
    }
};

QuirkList.prototype.formatValue = function(name, value) {
    var suffix = "";

    var prefix = "";
    if (value > 0) {
        prefix = "+";
    }
    else if (value < 0) {
        prefix = "\u2212";
        value = -value;
    }

    if (name.endsWith("%")) {
        name = name.slice(0, -2);
        suffix = "%";
        value *= 100.0;
    }
    else if (name.endsWith("+")) {
        name = name.slice(0, -2);
    }

    value = parseFloat(value).toPrecision(5).replace(/0+$/, '').replace(/\.$/, '');

    return [name, prefix + value + suffix];
};

///////////////////////////////////////////////////////////

function Mech(name, data) {
    this.name = name;
    this.data = data;
};

Mech.prototype.prefixEqual = function(other) {
    if (!other) {
        return false;
    }

    var othernames = other.name.split('-');
    var thisnames = this.name.split('-');

    var otherbase = othernames[0];
    if (othernames[1] == "IIC") {
        otherbase += "-IIC";
    }

    var thisbase = thisnames[0];
    if (thisnames[1] == "IIC") {
        thisbase += "-IIC";
    }

    return otherbase == thisbase;
};

Mech.prototype.collateQuirks = function(quirknames) {
    var quirks = {};
    for (var i of ["set", "centre_torso", "head",
                   "left_arm", "left_leg", "left_torso",
                   "right_arm", "right_leg", "right_torso"]) {
        if (i in this.data) {
            for (var q in this.data[i]) {
                var qn = q;
                if (quirknames) {
                    qn = quirknames.getName(q);
                }

                if (!(qn in quirks)) {
                    quirks[qn] = 0;
                }

                quirks[qn] += parseFloat(this.data[i][q]);
            }
        }
    }

    return quirks;
};

///////////////////////////////////////////////////////////

function Model() {
    this.version = "";
    this.date = "";
    this.quirklist = new QuirkList();
    this.mechlist = [];
    this.versions = [];

    this.urlver = getUrlParameter("v");

    this.loadState = 0;
};

Model.prototype.hookup = function() {
    var selector = document.querySelector("#verselect");
    selector.onchange = function() {
        if (selector.value) {
            window.location = "?v=" + selector.value;
        }
    };
};

Model.prototype.start = function() {
    var fetchVersion = (function(ver) {
        var baseurl = "json/" + ver;

        fetchJSON(baseurl + "/quirks.json", 10000, (function(data) {
            if (data && data.schema == 1) {
                this.version = data.version;
                this.date = data.date;

                for (var i in data.mechs) {
                    var mech = new Mech(i, data.mechs[i]);
                    this.mechlist.push(mech);
                }

                this.loadState++;
                this.checkLoadState();
            }
            else {
                this.loadError("quirks");
            }
        }).bind(this));

        fetchJSON(baseurl + "/quirknames.json", 10000, (function(data) {
            if (data) {
                for (var i in data) {
                    this.quirklist.addQuirk(i, data[i]);
                }

                this.loadState++;
                this.checkLoadState();
            }
            else {
                this.loadError("quirknames");
            }
        }).bind(this));
    }).bind(this);

    if (this.urlver && this.urlver.indexOf("..") == -1) {
        fetchVersion(this.urlver);
    }

    // fetch the index anyways to get the list
    fetchJSON("json/index.json", 5000, (function(data) {
        if (data) {
            for (var i in data) {
                this.versions.push([data[i].date, i]);
            }
            this.versions.sort().reverse();

            this.populateVersions(this);

            if (!this.urlver) {
                if (this.versions.length > 0) {
                    fetchVersion(this.versions[0][1]);
                }
                else {
                    this.loadError("index invalid");
                }
            }
        }
        else {
            this.loadError("index");
        }
    }).bind(this));
};

Model.prototype.removeSpinny = function() {
    var l = document.querySelector("#loader");
    l.parentElement.removeChild(l);
};

Model.prototype.loadError = function(err) {
    this.removeSpinny();

    // TODO: more fancy
    var base = document.querySelector("#content");
    base.style.fontSize = "18pt";
    base.style.textAlign = "center";

    base.textContent = "error loading: " + err;
};

Model.prototype.checkLoadState = function() {
    if (this.loadState != 2) {
        return;
    }

    // put mechs in sorted order
    this.mechlist.sort(function(a, b) {
        return a.name.localeCompare(b.name);
    });

    this.populatePage();
};

Model.prototype.populateVersions = function() {
    var selector = document.querySelector("#verselect");

    for (var i of this.versions) {
        var opt = document.createElement("option");
        opt.value = i[1];
        opt.text = i[1] + " (" + i[0] + ")";

        if (i[1] == this.urlver) {
            opt.selected = true;
        }

        selector.options.add(opt);
    }
};

Model.prototype.populatePage = function() {
    var base = document.querySelector("#content");

    base.style.visibility = "hidden";

    var headerTempl = document.querySelector("#header");
    var header = document.importNode(headerTempl.content, true);
    var fields = header.querySelectorAll("span");
    fields[0].textContent = this.date;
    fields[1].textContent = this.version;
    base.appendChild(header);

    function fixHeights(container) {
        if (container) {
            var list = container.querySelectorAll("div.mech");
            var maxheight = 0;
            for (var e of list) {
                var h = e.clientHeight;
                if (h > maxheight) {
                    maxheight = h;
                }
            }
            for (var e of list) {
                e.style.height = maxheight + "px";
            }
        }
    }

    var setTempl = document.querySelector("#mechsetTemplate");

    var maxheight = 0;
    var container = null;
    var lastmech = null;
    for (var i of this.mechlist) {
        if (!i.prefixEqual(lastmech)) {
            fixHeights(container, maxheight);

            container = document.importNode(setTempl.content, true);
            base.appendChild(container);
            container = base.lastElementChild;
            maxheight = 0;
        }

        var e = this.buildMech(i);
        container.appendChild(e);

        lastmech = i;
    }
    fixHeights(container, maxheight);

    this.removeSpinny();

    base.style.visibility = "visible";

    var selector = document.querySelector("#verselect");
    selector.style.visibility = "visible";
};

Model.prototype.buildMech = function(mech) {
    var tt = document.querySelector("#mechTemplate");
    var qt = document.querySelector("#quirkTemplate");

    var e = document.importNode(tt.content, true);
    e.id = mech.name;

    var mn = e.querySelector("div.mechname");
    mn.textContent = mech.name;

    var tb = e.querySelector("tbody");

    var quirks = mech.collateQuirks(this.quirklist);
    for (var i of Object.keys(quirks).sort()) {
        var v = this.quirklist.formatValue(i, quirks[i]);

        var qe = document.importNode(qt.content, true);
        var td = qe.querySelectorAll("td");
        td[0].textContent = v[0];
        td[1].textContent = v[1];

        tb.appendChild(qe);
    }

    if (Object.keys(quirks).length == 0) {
        var nothing = document.querySelector("#quirkNothing");
        var qe = document.importNode(nothing.content, true);
        tb.appendChild(qe);
    }

    return e;
};

///////////////////////////////////////////////////////////

window.addEventListener('load', function() {
    var model = new Model();
    model.hookup();
    model.start();
});
