#!/usr/bin/env python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import os
import sys
import decimal

def checkListSameness(d):
    if len(d.values()) <= 1:
        return True
    first = None
    for i in d.values():
        if first is None:
            first = i
        if first != i:
            return False
    return True

def getAnyInDict(d):
    for i in d.values():
        return i
    return None

class MechSkillTreeHandler(ContentHandler):
    def __init__(self):
        self.nodes = {}

        self.inNode = False

    def startElement(self, name, attrs):
        if name == "Node":
            col = int(attrs.getValue("column"))
            row = int(attrs.getValue("row"))

            self.inNode = True
            self.nodes[(col, row)] = attrs.getValue("name")

    def endElement(self, name):
        if name == "Node":
            if self.inNode:
                self.inNode = False

    def getSections(self):
        columns = map(lambda key: key[0], self.nodes)
        columns = sorted(set(columns))

        # find the runs
        runs = []
        startcol = columns.pop(0)
        lastcol = startcol
        rowzeros = []
        while columns:
            nextcol = columns.pop(0)
            if nextcol == (lastcol + 1):
                lastcol = nextcol
            else:
                runs.append( (startcol, lastcol) )
                startcol = nextcol
                lastcol = startcol
        runs.append( (startcol, lastcol) )

        # for each run, find the middle point
        sections = []
        for run in runs:
            rootcols = []
            for col in range(run[0], run[1]+1):
                rows = map(lambda key: key[1] if (key[0] == col) else None, self.nodes)
                rows = set(rows)
                if 0 in rows:
                    rootcols.append(col)
            if len(rootcols) != 3:
                raise
            sections.append( (run[0], rootcols[1], run[1]) )

        return sections

    def getNodesInSection(self, section):
        nodes = []
        for key in self.nodes:
            if (section[0] <= key[0]) and (key[0] <= section[2]):
                nodes.append(key)

        def sortTree(key):
            col = key[0] - section[1]
            if col % 2 == 1:
                return (2*key[1]+1, col)
            else:
                return (2*key[1], col)
        return sorted(nodes, key=sortTree)

class StringTable(ContentHandler):
    def __init__(self):
        self.mapping = {}
        self.nodes = []

        self.inRow = False
        self.inCell = False
        self.inData = False
        self.accum = ""

    def startElement(self, name, attrs):
        self.nodes.append(name)

        if len(self.nodes) == 4 and name == "Row":
            self.inRow = True
            self.inCell = False
            self.data = []

        if len(self.nodes) == 5 and self.inRow:
            if name == "Cell":
                if "ss:Index" in attrs and attrs.getValue("ss:Index") == "2":
                    self.inCell = True
                    self.inData = False
                elif len(self.data) > 0:
                    self.inCell = True
                    self.inData = False

        if len(self.nodes) == 6 and self.inCell:
            if name == "Data" and "ss:Type" in attrs and attrs.getValue("ss:Type") == "String":
                self.inData = True
                self.accum = ""

    def endElement(self, name):
        if len(self.nodes) == 6 and self.inData:
            if name == "Data":
                self.inData = False
                self.data.append(self.accum.strip())
                self.accum = ""

        if len(self.nodes) == 5 and self.inCell:
            if name == "Cell":
                self.inCell = False

        if len(self.nodes) == 4 and self.inRow:
            if name == "Row":
                self.inRow = False;
                if len(self.data) >= 3:
                    if self.data[0].startswith("qrk_") or self.data[0].startswith("EMechTreeNode_"):
                        self.mapping[self.data[0]] = self.data[1]

        self.nodes.pop()

    def characters(self, content):
        if self.inData:
            self.accum += content

    def getSkillName(self, skill):
        key = "EMechTreeNode_" + skill

        if key in self.mapping:
            return self.mapping[key]

        formattedname = ""
        while skill:
            if skill[0].isupper() and len(skill) > 1 and skill[1].islower():
                if len(formattedname) > 0 and formattedname[-1] != " ":
                    formattedname += " "
                formattedname += skill[0]
            elif skill[0].islower() and len(skill) > 1 and skill[1].isupper():
                formattedname += skill[0] + " "
            else:
                formattedname += skill[0]
            skill = skill[1:]
        return formattedname.strip()

class MechSkillEffects(ContentHandler):
    def __init__(self):
        self.skills = {}
        self.nodes = []

        self.inNode = False
        self.skillNames = []
        self.skillEffects = []

        self.inEffect = False
        self.savedValue = None
        self.effectName = None
        self.effectValue = None

        self.inFaction = False
        self.faction = {}
        self.factionName = None
        self.factionValue = None

        self.inWeight = False
        self.weightclass = {}
        self.weightName = None
        self.weightValue = None

        self.inTonnage = False
        self.tonnage = {}
        self.tonnageName = None
        self.tonnageValue = None

    def startElement(self, name, attrs):
        self.nodes.append(name)

        if len(self.nodes) == 2 and name == "Node":
            self.inNode = True
            self.skillNames = attrs.getValue("names").split(',')
            self.skillEffects = []
            self.effectName = None
            self.effectValue = None

        if len(self.nodes) == 3 and self.inNode and name == "Effect":
            self.inEffect = True
            self.savedValue = None
            self.effectName = attrs.getValue("name")
            self.effectValue = attrs.getValue("value")
            self.faction = {}
            self.factionName = None
            self.factionValue = None

        if len(self.nodes) == 4 and self.inEffect and name == "Faction":
            self.inFaction = True
            self.factionName = attrs.getValue("name")
            self.factionValue = attrs.getValue("value")
            self.weightclass = {}
            self.weightName = None
            self.weightValue = None

        if len(self.nodes) == 5 and self.inFaction and name == "WeightClass":
            self.inWeight = True
            self.weightName = attrs.getValue("name");
            self.weightValue = attrs.getValue("value")
            self.tonnage = {}
            self.tonnageName = None
            self.tonnageValue = None

        if len(self.nodes) == 6 and self.inWeight and name == "Tonnage":
            self.inTonnage = True
            self.tonnageName = attrs.getValue("name")
            self.tonnageValue = attrs.getValue("value")

    def endElement(self, name):
        # TODO:
        # - just make it very verbose? if there isn't something, write it?
        # - that would make every effect be 34 entries

        # if tonnage, take the very last one in the WeightClass...
        # { value: 1 }
        # { value: [IS: 1, Clan: 1] }
        # { value: [IS: [Light: 1, Medium: 1, Heavy: 1, Assault: 1], Clan: 1] }

        if len(self.nodes) == 6 and self.inTonnage and name == "Tonnage":
            self.inTonnage = False
            self.tonnage[self.tonnageName] = self.tonnageValue

        if len(self.nodes) == 5 and self.inWeight and name == "WeightClass":
            self.inWeight = False
            if self.tonnage:
                value = self.tonnage[max(self.tonnage, key=float)]
            else:
                value = self.weightValue

            self.weightclass[self.weightName] = value

        if len(self.nodes) == 4 and self.inFaction and name == "Faction":
            self.inFaction = False
            if not checkListSameness(self.weightclass):
                value = self.weightclass
                if len(value) != 4:
                    value['_'] = self.factionValue
            elif len(self.weightclass) > 1 and type(getAnyInDict(self.weightclass)) is dict:
                value = {'_': getAnyInDict(self.weightclass)}
            elif self.weightclass:
                value = getAnyInDict(self.weightclass)
            else:
                value = self.factionValue

            self.faction[self.factionName] = value

        if len(self.nodes) == 3 and self.inEffect and name == "Effect":
            self.inEffect = False
            if not checkListSameness(self.faction):
                value = self.faction
                if len(value) != 2:
                    value['_'] = self.effectValue
            elif len(self.faction) > 1 and type(getAnyInDict(self.faction)) is dict:
                value = {'_': getAnyInDict(self.faction)}
            elif self.faction:
                value = getAnyInDict(self.faction)
            else:
                value = self.effectValue

            self.skillEffects.append((self.effectName, value))

        if len(self.nodes) == 2 and self.inNode and name == "Node":
            self.inNode = False

            for n in self.skillNames:
                self.skills[n] = self.skillEffects

        self.nodes.pop()

    def getSkillValue(self, skill):
        effects = self.skills[skill]

        if len(effects) <= 3:
            name, value = effects[0]

            if name.endswith('_multiplier'):
                converter = lambda v: str(decimal.Decimal(v) * 100).rstrip("0").rstrip(".")
            elif name.endswith('_additive'):
                converter = lambda v: v.rstrip("0").rstrip(".")
            else:
                return 0

            def stringify(d):
                if type(d) is dict:
                    orderedlist = ['_', 'Clan', 'IS', 'Light', 'Medium', 'Heavy', 'Assault']
                    out = []
                    for k in orderedlist:
                        if k in d:
                            out.append('"' + k + '": ' + stringify(d[k]))
                    for k in d:
                        if k not in orderedlist:
                            out.append('"' + k + '": ' + stringify(d[k]))
                    return "{" + ', '.join(out) + "}"
                else:
                    return converter(d)

            return stringify(value)
        else:
            return 1

def convertNode(node, strings):
    skillname = skillnodes.nodes[node]

    rawname = skillname.rstrip("0123456789")

    name = strings.getSkillName(skillname)

    level = ""
    while name[-1].isdigit():
        level = name[-1] + level
        name = name[:-1]
    name = name.strip()
    if not level:
        level = "0"

    mapping = {
            'Adv. Salvos': 'Advanced Salvos',
            'FLAMER VENTILATION': 'Flamer Ventilation',
            'ENHANCED UAC/RAC': 'Enhanced UAC/RAC',
    }
    if name in mapping:
        name = mapping[name]

    return (skillname, rawname, name, level)

# my own formatter to make it easy for me to diff
def formatMyJson(version, skillnodes, skilleffects, strings):
    print('{')
    print('  "date" : "TODO_DATE",')
    print('  "version" : "{}",'.format(version))
    print('  "sections" : [')

    sections = skillnodes.getSections()
    while sections:
        section = sections.pop(0)

        print('      {')
        print('        "category" : "TODO_CATG",')
        print('        "title" : "TODO_TITLE",')
        print('        "descr" : "",')
        print('        "cells" : [')

        nodes = skillnodes.getNodesInSection(section)
        while nodes:
            node = nodes.pop(0)
            skillname, rawname, formattedname, level = convertNode(node, strings)

            col, row = node
            col -= section[1]
            if col % 2 == 1:
                row += 1

            print('            {')
            print('              "col" : {},'.format(col))
            print('              "row" : {},'.format(row))
            print('              "type" : "{}",'.format(formattedname))
            print('              "level" : {},'.format(level))

            value = skilleffects.getSkillValue(skillname)
            print('              "value" : {},'.format(value))

            print('              "links" : [')

            if col % 2 == 0:
                nextrow = row+1
            else:
                nextrow = row

            print('                  {{ "col" : {}, "row" : {} }},'.format(col-1, nextrow))
            print('                  {{ "col" : {}, "row" : {} }},'.format(col, row+1))
            print('                  {{ "col" : {}, "row" : {} }}'.format(col+1, nextrow))

            print('                ]')
            if nodes:
                print('            },')
            else:
                print('            }')

        print('          ]')
        if sections:
            print('      },')
        else:
            print('      }')

    print('    ]')
    print('}')


if __name__ == '__main__':
    if len(sys.argv) < 3:
        print("usage: {} <ver> <datapath>".format(sys.argv[0]))
        sys.exit(1)

    version = sys.argv[1]
    workpath = sys.argv[2]

    saxparser = make_parser()

    # read in the skill tree
    skillnodes = MechSkillTreeHandler()
    saxparser.setContentHandler(skillnodes)
    saxparser.parse(open(os.path.join(workpath, "MechSkillTreeNodesDisplay.xml"), "r"))

    # read in the string mapping
    strings = StringTable()
    saxparser.setContentHandler(strings)
    saxparser.parse(open(os.path.join(workpath, "TheRealLoc.xml"), "r"))

    # read in the skill/effect mapping
    skilleffects = MechSkillEffects()
    saxparser.setContentHandler(skilleffects)
    saxparser.parse(open(os.path.join(workpath, "MechSkillTreeNodes.xml"), "r"))

    formatMyJson(version, skillnodes, skilleffects, strings)
