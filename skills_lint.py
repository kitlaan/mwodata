#!/usr/bin/env python3

import json
import sys
from copy import deepcopy

if len(sys.argv) < 2:
    print("usage: {} <skilljson> [priorjson]".format(sys.argv[0]))
    sys.exit(1)

data = json.load(open(sys.argv[1]))

if len(sys.argv) > 2:
    previous = json.load(open(sys.argv[2]))

    # compare the various sections to see if there's any major changes
    if len(data["sections"]) != len(previous["sections"]):
        print("section count mismatch")
        sys.exit(2)

    for i in range(len(data["sections"])):
        old = previous["sections"][i]
        new = data["sections"][i]

        if new["category"] == "TODO_CATG":
            new["category"] = old["category"]
        if new["title"] == "TODO_TITLE":
            new["title"] = old["title"]
        if new["descr"] == "":
            new["descr"] = old["descr"]

        if len(old["cells"]) != len(new["cells"]):
            print("section {} cells mismatch".format(new["title"]))
            sys.exit(2)

        for j in range(len(new["cells"])):
            cold = old["cells"][j]
            cnew = new["cells"][j]

            if cold["col"] == cnew["col"] and cold["row"] == cnew["row"] and \
                    cold["type"] == cnew["type"] and cold["level"] == cnew["level"] and \
                    len(cnew["links"]) == 3:
                cnew["links"] = deepcopy(cold["links"])


print('{')
print('  "date" : "{}",'.format(data["date"]))
print('  "version" : "{}",'.format(data["version"]))
print('  "sections" : [')

sections = data["sections"]
while sections:
    section = sections.pop(0)

    print('      {')
    print('        "category" : "{}",'.format(section["category"]))
    print('        "title" : "{}",'.format(section["title"]))
    print('        "descr" : "{}",'.format(section["descr"]))
    print('        "cells" : [')

    def sortTree(cell):
        col = cell["col"]
        if col % 2 == 0:
            return (2*cell["row"]+1, col)
        else:
            return (2*cell["row"], col)

    nodes = sorted(section["cells"], key=sortTree)
    while nodes:
        node = nodes.pop(0)

        print('            {')
        print('              "col" : {},'.format(node["col"]))
        print('              "row" : {},'.format(node["row"]))
        print('              "type" : "{}",'.format(node["type"]))
        print('              "level" : {},'.format(node["level"]))

        def stringify(d):
            if type(d) is dict:
                orderedlist = ['_', 'Clan', 'IS', 'Light', 'Medium', 'Heavy', 'Assault']
                out = []
                for k in orderedlist:
                    if k in d:
                        out.append('"' + k + '": ' + stringify(d[k]))
                for k in d:
                    if k not in orderedlist:
                        out.append('"' + k + '": ' + stringify(d[k]))
                return "{" + ', '.join(out) + "}"
            else:
                return str(d)

        print('              "value" : {},'.format(stringify(node["value"])))

        print('              "links" : [')

        links = sorted(node["links"], key=lambda n: n["col"])
        while links:
            link = links.pop(0)
            print('                  {{ "col" : {}, "row" : {} }}{}'.format(link["col"], link["row"], ',' if links else ''))

        print('                ]')
        if nodes:
            print('            },')
        else:
            print('            }')

    print('          ]')
    if sections:
        print('      },')
    else:
        print('      }')

print('    ]')
print('}')
