#!/bin/bash

DATAFILES="$1"

# HACK: if provided a file, treat it as a tarfile
if [ -f "$DATAFILES" ]; then
    TEMPDIR=$(mktemp -d)
    tar xf "$DATAFILES" -C $TEMPDIR
    if [ $? -ne 0 ]; then
        rm -rf $TEMPDIR
        exit 1
    fi
    DATAFILES="$TEMPDIR/*"
fi

if [ ! -d "$DATAFILES" ]; then
    echo "invalid data dir '$DATAFILES'"
    exit 1
fi

if [ -z "$VERSION" ]; then
    VERSION=$(xmllint --xpath 'string(//BuildVersion)' $DATAFILES/build_info.xml)
fi
if [ -z "$VERSION" ]; then
    VERSION="unkown"
fi

OUTDIR=json_new/$VERSION
mkdir -p "$OUTDIR"

# run conversions
./convert_quirks.py $VERSION "$DATAFILES" >"$OUTDIR/quirks.json"
./convert_quirknames.py "$DATAFILES" >"$OUTDIR/quirknames.json" 2>"$OUTDIR/quirknames.err"
./convert_skills.py $VERSION "$DATAFILES" >"$OUTDIR/skills.json"
./convert_skillnames.py "$DATAFILES" >"$OUTDIR/skillnames.json"

if [ ! -s "$OUTDIR/quirknames.err" ]; then
    rm "$OUTDIR/quirknames.err"
fi

if [ -n "$TEMPDIR" ]; then
    rm -rf "$TEMPDIR"
fi
