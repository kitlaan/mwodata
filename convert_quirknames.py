#!/usr/bin/env python3

import json
import os
import sys
from collections import OrderedDict
import xml.etree.ElementTree as ET

ADJTYPES = OrderedDict([
        ('additive', '+'),
        ('multiplier', '%'),
])

# TODO: fix the sorting order
MODULES = OrderedDict([
        ('hd', '(HD)'),
        ('ct', '(CT)'),
        ('lt', '(LT)'),
        ('rt', '(RT)'),
        ('la', '(LA)'),
        ('ra', '(RA)'),
        ('ll', '(LL)'),
        ('rl', '(RL)'),
        ('cooldown', 'Cooldown'),
        ('spread', 'Spread'),
        ('velocity', 'Velocity'),
        ('duration', 'Duration'),
        ('heat', 'Heat Generation'),
        ('range', 'Range'),
        ('jamchance', 'Jam Chance'),
        ('jamrampdownduration', 'Jam Rampdown Duration'),
        ('ammocapacity', 'Ammo Capacity'),
        ('jumpjetslots', 'Jump Jet Capacity'),
        ('heatdissipation', 'Heat Dissipation'),
        ('heatloss', 'Heat Dissipation'),
        ('externalheat', 'External Heat Transfer'),
        ('stealtharmorcooldown', 'Stealth Armor Cooldown'),
        ('overheatdamage', 'Overheat Damage'),
        ('falldamage', 'Fall Damage'),
        ('minheatpenaltylevel', 'Min Heat Penalty Level'),
        ('narcduration', 'Tag Duration'),
        ('targetdecayduration', 'Target Decay Duration'),
        ('targetinfogathering', 'Target Info Gathering'),
        ('backfacetargetretentionrange', 'Back Target Retention Range'),
        ('sensorrange', 'Sensor Range'),
        ('radardeprivation', 'Radar Deprivation'),
        ('pitchspeed', 'Pitch Speed'),
        ('yawspeed', 'Yaw Speed'),
        ('reversespeed', 'Reverse Speed'),
        ('mechtopspeed', 'Top Speed'),
        ('mechacceleration', 'Mech Acceleration'), # see also accellerp ?!?
        ('mechdeceleration', 'Mech Deceleration'), # see also decellerp ?!?
        ('turnrate', 'Turn Rate'), # see also turnlerp ?!?
        ('pitchangle', 'Pitch Angle'),
        ('yawangle', 'Yaw Angle'),
        ('initialthrust', 'Initial Thrust'),
        ('burntime', 'Burn Time'),
        ('hillclimb', 'Hill Climb'),
        ('rof', 'Rate of Fire'),
        ('receiving', '(Receiving)'),
        ('captureaccelerator', 'Capture Accelerator'),
        ('seismicsensorrange', 'Seismic Sensor Range'),
        ('uavrange', 'UAV Range'),
        ('uavcapacity', 'UAV Capacity'),
        ('extraconsumableslot', 'Extra Consumable Slot'),
        ('xpbonus', 'XP Bonus'),
        ('all', ''),
])

# TODO: fix the sorting order
SPECS = OrderedDict([
        ('energy', 'Energy Weapon'),
        ('laser', 'Laser'),
        ('clanlaser', 'Clan Laser'),
        ('nonpulselaser', 'Stadard and ER Laser'),
        ('isstdlaser', 'IS Standard Laser'),
        ('islargelaser', 'IS Large Laser'),
        ('ismediumlaser', 'IS Medium Laser'),
        ('pulselaser', 'Pulse Laser'),
        ('mediumpulselaser', 'Medium Pulse Laser'),
        ('islargepulselaser', 'IS Large Pulse Laser'),
        ('ismediumpulselaser', 'IS Medium Pulse Laser'),
        ('clanpulselaser', 'Clan Pulse Laser'),
        ('clanlargepulselsr', 'Clan Large Pulse Laser'),
        ('clanmediumpulselaser', 'Clan Medium Pulse Laser'),
        ('erlaser', 'ER Laser'),
        ('iserlaser', 'IS ER Laser'),
        ('isermediumlaser', 'IS ER Medium Laser'),
        ('iserlargelaser', 'IS ER Large Laser'),
        ('clanerlaser', 'Clan ER Laser'),
        ('clanerlargelaser', 'Clan ER Large Laser'),
        ('clanermediumlaser', 'Clan ER Medium Laser'),
        ('heavylaser', 'Heavy Laser'),
        ('clanheavylargelsr', 'Clan Heavy Large Laser'),
        ('clanheavymediumlaser', 'Clan Heavy Medium Laser'),
        ('ppcfamily', 'PPC (Family)'),
        ('iserppc', 'IS ER PPC'),
        ('clanerppc', 'Clan ER PPC'),
        ('ppc', 'PPC'), # pretty sure this is "just PPC" vs "any PPC"
        ('erppc', 'ER PPC'),
        ('isppc', 'IS PPC (Family)'),
        ('isstandardppc', 'IS PPC'),
        ('clanppc', 'Clan PPC'),
        ('issnubnoseppc', 'IS Snub Nose PPC'),
        ('islightppc', 'IS Light PPC'),

        ('ballistic', 'Ballistic Weapon'),
        ('autocannon', 'AC'),
        ('isautocannon', 'IS AC'),
        ('ac', 'AC'),
        ('ac20', 'AC/20'),
        ('ac10', 'AC/10'),
        ('ac5', 'AC/5'),
        ('ac2', 'AC/2'),
        ('isautocannon20', 'IS AC/20'),
        ('isautocannon10', 'IS AC/10'),
        ('cac20', 'Clan AC/20'),
        ('cac10', 'Clan AC/10'),
        ('cac5', 'Clan AC/5'),
        ('cac2', 'Clan AC/2'),
        ('isautocannon5', 'IS AC/5'),
        ('isautocannon2', 'IS AC/2'),
        ('ultraac20', 'Ultra AC/20'),
        ('ultraac10', 'Ultra AC/10'),
        ('ultraac5', 'Ultra AC/5'),
        ('ultraac2', 'Ultra AC/2'),
        ('ultraautocannon', 'Ultra AC'),
        ('ultraautocannon20', 'Ultra AC/20'),
        ('isultraautocannon', 'IS Ultra AC'),
        ('isultraautocannon5', 'IS Ultra AC/5'),
        ('cultraac20', 'Clan Ultra AC/20'),
        ('cultraac10', 'Clan Ultra AC/10'),
        ('clanultraautocannon5', 'Clan Ultra AC/5'),
        ('cultraac5', 'Clan Ultra AC/5'),
        ('clanultraautocannon2', 'Clan Ultra AC/2'),
        ('cultraac2', 'Clan Ultra AC/2'),
        ('lbxautocannon', 'LB-X AC'),
        ('lb10x', 'LB10-X AC'),
        ('lb5x', 'LB5-X AC'),
        ('lb2x', 'LB2-X AC'),
        ('islbxautocannon', 'IS LB-X AC'),
        ('islbxautocannon10', 'IS LB10-X AC'),
        ('clanlbxautocannon', 'Clan LB-X AC'),
        ('clb20x', 'Clan LB20-X AC'),
        ('clanlbxautocannon10', 'Clan LB10-X AC'),
        ('clb10x', 'Clan LB10-X AC'),
        ('clb5x', 'Clan LB5-X AC'),
        ('clb2x', 'Clan LB2-X AC'),
        ('gauss', 'Gauss'), # No clue why they didn't reuse 'gaussrifle', but shrug
        ('gaussrifle', 'Gauss'),
        ('lightgauss', 'Light Gauss'),
        ('isgaussrifle', 'IS Gauss'),
        ('clangaussrifle', 'Clan Gauss'),
        ('cgauss', 'Clan Gauss'),
        ('rotaryautocannon', 'RAC'),
        ('rotary_ac5', 'RAC/5'),
        ('rotary_ac2', 'RAC/2'),

        ('missile', 'Missile Weapon'),
        ('rocketlauncher', 'Rocket Launcher'),
        ('rocketlauncher15', 'Rocket Launcher 15'),
        ('lrm', 'LRM'),
        ('lrm_artemis', 'Artemis LRM'),
        ('islrm', 'IS LRM'),
        ('islrm20', 'IS LRM 20'),
        ('islrm15', 'IS LRM 15'),
        ('islrm10', 'IS LRM 10'),
        ('islrm5', 'IS LRM 5'),
        ('clanlrm', 'Clan LRM'),
        ('clrm', 'Clan LRM'),
        ('clrm_artemis', 'Clan Artemis LRM'),
        ('clanlrm20', 'Clan LRM 20'),
        ('mrm', 'MRM'),
        ('mrm10', 'MRM 10'),
        ('atm', 'ATM'),
        ('catm', 'Clan ATM'),
        ('srm', 'SRM'),
        ('srm_artemis', 'Artemis SRM'),
        ('csrm', 'Clan SRM'),
        ('csrm_artemis', ' Clan Artemis SRM'),
        ('issrm', 'IS SRM'),
        ('issrm6', 'IS SRM 6'),
        ('issrm4', 'IS SRM 4'),
        ('clansrm', 'Clan SRM'),
        ('clansrm2', 'Clan SRM 2'),
        ('streaksrm', 'Streak SRM'),
        ('streak_srm', 'Streak SRM'),
        ('isstreaksrm', 'IS Streak SRM'),
        ('isstreaksrm2', 'IS Streak SRM 2'),
        ('clanstreaksrm', 'Clan Streak SRM'),
        ('cstreak_srm', 'Clan Stream SRM'),

        ('narcbeacon', 'NARC Beacon'),
        ('narc', 'NARC Beacon'),
        ('isnarcbeacon', 'IS NARC Beacon'),
        ('clannarcbeacon', 'Clan NARC Beacon'),
        ('cnarc', 'Clan NARC Beacon'),

        ('isantimissilesystem', 'IS AMS'),
        ('clanantimissilesystem', 'Clan AMS'),

        ('machinegun', 'MG'),
        ('ismachinegun', 'IS MG'),
        ('clanmachinegun', 'Clan MG'),
        ('cmachinegun', 'Clan MG'),
        ('heavymachinegun', 'Heavy MG'),
        ('lightmachinegun', 'Light MG'),
        ('cheavymachinegun', 'Clan Heavy MG'),
        ('clightmachinegun', 'Clan Light MG'),
        ('isflamer', 'IS Flamer'),

        ('accellerp', 'Mech Acceleration'), # see also mechacceleration ?!?
        ('decellerp', 'Mech Deceleration'), # see also mechdeceleration ?!?
        ('turnlerp', 'Turn Rate'), # see also turnrate ?!?

        ('armorresist', 'Base Armor'),
        ('internalresist', 'Base Structure'),

        ('jumpjets', 'Jump Jets'),

        ('arm', 'Arm'),
        ('torso', 'Torso'),
        ('critchance', 'Crit Hit Chance'),
        ('all', ''),
])


def quirkSort(a, b):
    _, aS, aM, aA = a
    _, bS, bM, bA = b

    if not aS and bS:
        return 1
    if aS and not bS:
        return -1
    if aS and bS:
        if SPECS.keys().index(aS) < SPECS.keys().index(bS):
            return -1
        if SPECS.keys().index(aS) > SPECS.keys().index(bS):
            return 1
    if MODULES.keys().index(aM) < MODULES.keys().index(bM):
        return -1
    if MODULES.keys().index(aM) > MODULES.keys().index(bM):
        return 1
    if ADJTYPES.keys().index(aA) < ADJTYPES.keys().index(bA):
        return -1
    if ADJTYPES.keys().index(aA) > ADJTYPES.keys().index(bA):
        return 1
    return 0

def printerr(*objs):
    print(*objs, file=sys.stderr)

def loadMechQuirks(filepath):
    quirks = {}
    try:
        for q in ET.parse(filepath).getroot().iter('Quirk'):
            quirks[q.get('name')] = q.get('value')
    except:
        pass
    return set(quirks.keys())

def mapQuirk(name):
    quirk = name.split('_')
    if len(quirk) == 2:
        spec = None
        module, adjtype = quirk
    elif len(quirk) == 3:
        spec, module, adjtype = quirk
    elif len(quirk) > 3:
        spec = quirk[0]
        module = '_'.join(quirk[1:-1])
        adjtype = quirk[-1]

    try:
        # data hackery for my own sanity...
        if spec == 'ammocapacity':
            spec, module = [module, spec]


        m = MODULES[module]
        a = ADJTYPES[adjtype]
        s = SPECS[spec] if spec else None

        return (name, spec, module, adjtype)
    except:
        try:
            printerr('unknown: {} :: {} {} {}'.format(name, spec, module, adjtype))
        except:
            printerr('unknown: {}'.format(name))

def formatQuirk(quirk):
    name, spec, mod, adj = quirk

    field = ''
    if spec and SPECS[spec]:
        field = field + ' ' + SPECS[spec]
    if mod and MODULES[mod]:
        field = field + ' ' + MODULES[mod]
    if adj and ADJTYPES[adj]:
        field = field + ' ' + ADJTYPES[adj]

    return (name, field.strip())

if __name__ == '__main__':
    if len(sys.argv) != 2:
        print("{} <dir>".format(sys.argv[0]))
        sys.exit(1)

    workpath = sys.argv[1]

    quirks = set()
    for entry in os.scandir(workpath):
        if entry.is_file() and (entry.name.endswith('.mdf') or entry.name.endswith('.xml')):
            quirks.update(loadMechQuirks(os.path.join(workpath, entry.name)))

    quirkmap = {}
    for q in quirks:
        m = mapQuirk(q)
        if m:
            name, field = formatQuirk(m)
            quirkmap[name] = field

    print(json.dumps(quirkmap, indent=2, sort_keys=True))
