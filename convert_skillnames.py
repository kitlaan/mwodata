#!/usr/bin/env python3

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
import os
import sys

class StringTable(ContentHandler):
    def __init__(self):
        self.mapping = {}
        self.nodes = []

        self.inRow = False
        self.inCell = False
        self.inData = False
        self.accum = ""

    def startElement(self, name, attrs):
        self.nodes.append(name)

        if len(self.nodes) == 4 and name == "Row":
            self.inRow = True
            self.inCell = False
            self.data = []

        if len(self.nodes) == 5 and self.inRow:
            if name == "Cell":
                if "ss:Index" in attrs and attrs.getValue("ss:Index") == "2":
                    self.inCell = True
                    self.inData = False
                elif len(self.data) > 0:
                    self.inCell = True
                    self.inData = False

        if len(self.nodes) == 6 and self.inCell:
            if name == "Data" and "ss:Type" in attrs and attrs.getValue("ss:Type") == "String":
                self.inData = True
                self.accum = ""

    def endElement(self, name):
        if len(self.nodes) == 6 and self.inData:
            if name == "Data":
                self.inData = False
                self.data.append(self.accum.strip())
                self.accum = ""

        if len(self.nodes) == 5 and self.inCell:
            if name == "Cell":
                self.inCell = False

        if len(self.nodes) == 4 and self.inRow:
            if name == "Row":
                self.inRow = False;
                if len(self.data) >= 3:
                    if self.data[0].startswith("EMechTreeNode_") and self.data[0].endswith("_desc"):
                        stripped = self.data[0][14:-5].rstrip('1234567890')
                        if stripped not in self.mapping:
                            self.mapping[stripped] = self.data[1]

        self.nodes.pop()

    def characters(self, content):
        if self.inData:
            self.accum += content

if len(sys.argv) < 2:
    print("usage: {} <datapath>".format(sys.argv[0]))
    sys.exit(1)

workpath = sys.argv[1]

saxparser = make_parser()

# read in the string mapping
strings = StringTable()
saxparser.setContentHandler(strings)
saxparser.parse(open(os.path.join(workpath, "TheRealLoc.xml"), "r"))

formatted = {}
for key in strings.mapping:
    name = key
    formattedname = ""
    while name:
        if name[0].isupper() and len(name) > 1 and name[1].islower():
            if len(formattedname) > 0 and formattedname[-1] != " ":
                formattedname += " "
            formattedname += name[0]
        elif name[0].islower() and len(name) > 1 and name[1].isupper():
            formattedname += name[0] + " "
        else:
            formattedname += name[0]
        name = name[1:]
    formattedname = formattedname.strip()

    replacers = [
            ('Auto Cannon', 'AC'),
            ('Gauss Rifle', 'Gauss'),
            ('LBX Auto Cannon', 'LBX'),
            ('Ultra Auto Cannon Jamchance', 'Enhanced UAC/RAC'),
            ('Ultra Auto Cannon', 'UAC'),
            ]

    for r in replacers:
        if formattedname == r[0] or formattedname.startswith(r[0] + ' '):
            formattedname = formattedname.replace(r[0], r[1], 1)
            break

    formatted[formattedname] = strings.mapping[key]

sortedkeys = sorted(formatted, key=lambda s: s.lower())

def keyType(key):
    NUMBER_TYPES = ["Advanced Zoom", "AMS Overload", "Consumable Slot",
            "Coolant Reserves", "Expanded Reserves", "Extra UAV",
            "Gauss Charge", "Magazine Capacity", "Missile Rack",
            "Seismic Sensor", "Target Decay", "Target Retention",
            "UAV Time", "UAV Time On Station"]
    if key in NUMBER_TYPES:
        return "number"
    return "percent"

print('{')
for key in sortedkeys:
    print('    "{}": {{'.format(key))
    #print('        "type": "percent",')
    print('        "type": "{}",'.format(keyType(key)))
    print('        "descr": "{}"'.format(formatted[key]))
    print('    }}{}'.format('' if key == sortedkeys[-1] else ','))
print('}')
