#!/bin/bash

DATAFILES="$1"

if [ -e workdir ]; then
    echo "workdir exists..."
    exit 1
fi

if [ ! -d "$DATAFILES" ]; then
    echo "invalid data dir '$DATAFILES'"
    exit 1
fi

mkdir workdir

echo "extracting..."

7z e "$DATAFILES"/Game/Objects.pak -oworkdir *.mdf *-omnipods.xml -r
for file in "$DATAFILES"/Game/mechs/*.pak; do
    7z e "$file" -oworkdir *.mdf *-omnipods.xml -r
done

7z e "$DATAFILES"/Game/Localized/English.pak -oworkdir Languages/TheRealLoc.xml

7z e "$DATAFILES"/Game/GameData.pak -oworkdir Libs/MechPilotTalents/MechSkillTreeNodes*.xml

cp "$DATAFILES"/build_info.xml workdir

xmllint --xpath 'string(//BuildVersion)' workdir/build_info.xml
echo
